#HW_L8_T1_CustomTreeMapImpl

Реализовать класс CustomTreeMapImpl<K,V>, который представляет отображение на основе бинарного дерева поиска.

Класс CustomTreeMapImpl реализует интерфейс CustomTreeMap<K,V>
Класс CustomTreeMapImpl может хранить объекты любого типа

##Конструкторы

CustomTreeMapImpl(Comparator<K> comparator);

Ссылка на CustomTreeMap<K,V>: #ref

#Критерии приемки

1. Создать ветку feature/CustomTreeMap
2. Добавить интерфейс CustomTreeMap в ветку, сделать PUSH в удаленный репозиторий

3. Создать ветку feature/CustomTreeMapImpl от ветки feature/CustomTreeMap

4. Написать реализацию класса CustomTreeMapImpl

5. Предоставить на проверку Pull Request из ветки feature/CustomTreeMapImpl в ветку feature/CustomTreeMap

6. Каждый публичный метод класса CustomTreeMapImpl должен быть покрыт unit тестом

7. !!! Вносить правки в интерфейс CustomTreeMap<K,V> нельзя

